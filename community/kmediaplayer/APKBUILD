# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmediaplayer
pkgver=5.106.0
pkgrel=0
pkgdesc="Media player framework for KDE 5"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="X11 AND LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev kparts-dev kxmlgui-dev"
makedepends="$depends_dev extra-cmake-modules samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kmediaplayer-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# viewtest requires X11 to be running
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
ececdc4851bbdfa07966e87d6db8c44c6df877c119068315712dddca254a874fd72b5b78186576d61119fc3fb47fc4f523531723a11cfcb75e47312b62505e6c  kmediaplayer-5.106.0.tar.xz
"
