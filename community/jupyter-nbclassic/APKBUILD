# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=jupyter-nbclassic
pkgver=0.5.6
pkgrel=0
pkgdesc="Jupyter notebook as jupyter server extension"
url="https://github.com/jupyter/nbclassic"
# s390x, ppc64le: no jupyter-server
arch="noarch !armhf !s390x !ppc64le"
license="BSD-3-Clause"
depends="py3-traitlets jupyter-server jupyter-notebook-shim"
makedepends="py3-gpep517 py3-jupyter-packaging"
checkdepends="py3-pytest py3-pytest-tornasync py3-pytest-jupyter"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jupyter/nbclassic/releases/download/v$pkgver/nbclassic-$pkgver.tar.gz"
builddir="$srcdir/nbclassic-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
e259edb862a870c2b992c6c2c2d8ead64fc69f98298bd1830d63f1db001f8b36ab9509a693b5e425dcc2a43eb72b256f1082b1f0c66757bdbf35968175eb58c7  jupyter-nbclassic-0.5.6.tar.gz
"
