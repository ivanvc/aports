# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
pkgver=2023.05.22
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-lang/rust-analyzer"
# armhf, armv7, x86: some tests fail, not supported by upstream
# riscv64, s390x: blocked by cargo/rust
arch="aarch64 ppc64le x86_64"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo mimalloc2-dev cargo-auditable"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="https://github.com/rust-lang/rust-analyzer/archive/$_pkgver/rust-analyzer-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"
# requires rustup toolchain to run rustup rustfmt for sourcegen
options="net !check"

# crashes otherwise
unset CARGO_PROFILE_RELEASE_PANIC


prepare() {
	default_prepare

	# rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# build against system-provided mimalloc.
	# newline because file doesn't end in newline..
	cat >> .cargo/config.toml <<-EOF

		[target.$target]
		mimalloc = { rustc-link-lib = ["mimalloc"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	CFG_RELEASE="$pkgver" \
		cargo auditable build \
		--frozen \
		--release \
		--features="mimalloc"
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rust-analyzer -t "$pkgdir"/usr/bin/
	install -Dm644 docs/user/manual.adoc -t "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="
43a25731f0b6d0dadda5e8e395a98180d0b0ad416858618f5f60df95a24629ac8afd23deef4e5284fd7cb5af4b694c617a0e345ba0f8b88efeb401c6fcff804b  rust-analyzer-2023.05.22.tar.gz
"
