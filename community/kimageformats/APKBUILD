# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kimageformats
pkgver=5.106.0
pkgrel=0
pkgdesc="Image format plugins for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	libavif-dev
	libheif-dev
	libraw-dev
	openexr-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kimageformats-$pkgver.tar.xz"

case "$CARCH" in
s390x)
	;;
*)
	makedepends="$makedepends libjxl-dev"
	;;
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKIMAGEFORMATS_HEIF=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kimageformats-read-psd"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e58e742f916784889efc201a1eb4c759d2b88c9c50825ef2cb6d4e776f502387e054f031d9ca4401285dbc928fb6347783346fcffce2851d7a22589f5d42c7b7  kimageformats-5.106.0.tar.xz
"
