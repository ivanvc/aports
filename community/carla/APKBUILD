# Contributor: Magnus Sandin <magnus.sandin@gmail.com>
# Maintainer: Magnus Sandin <magnus.sandin@gmail.com>
pkgname=carla
pkgver=2.5.4
pkgrel=5
pkgdesc="Fully-featured audio plugin host"
url="https://github.com/falkTX/Carla"
options="!check" # No tests available
arch="armv7 aarch64 x86 x86_64"
license="GPL-2.0-or-later"
depends="
	python3
	py3-pyliblo
	py3-qt5
	py3-rdflib
	qt5-qtsvg
	"
depends_dev="$pkgname"
makedepends="
	alsa-lib-dev
	file-dev
	fluidsynth-dev
	liblo-dev
	libsndfile-dev
	libx11-dev
	linux-headers
	musl-fts-dev
	pulseaudio-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/falkTX/Carla/archive/refs/tags/v$pkgver.tar.gz
	sh-launcher.patch
	"
builddir="$srcdir/Carla-$pkgver"

build() {
	make features
	LDFLAGS="$LDFLAGS -l fts" make
}

package() {
	make install PREFIX=/usr DESTDIR="$pkgdir"
}

sha512sums="
0bee1b450d832fafa035694f16fdae0509a2836449da875f1fe6e81adb2bbd4747e8c42e516c3896d0af03f50a47c1d3b63119bd321d10464c1c5e57645461b4  carla-2.5.4.tar.gz
8a4dccbd0e11ab8c3f348659b507ac3cb9597e8700b93d1a28d07fc5c6cd990dcadf5be485570677bf26e97eff5e3e71689935ba5f892dbe246ab1abe3de9899  sh-launcher.patch
"
