# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-mime
pkgver=23.04.1
pkgrel=0
pkgdesc="Libraries and daemons to implement basic email handling"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by akonadi
# ppc64le blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-dev>=$pkgver
	kcodecs-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	kmime-dev
	kxmlgui-dev
	libxslt-dev
	qt5-qtbase-dev
	shared-mime-info
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-mime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# mailserializerplugintest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "mailserializerplugintest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4c51f9185ba2a04e14fa77d4ed7d327266ee8cf9f21f0723f17c1fa35518b3d2ab95d0ee4c4a34351f987869d3b4fa36cf65f80725102364c6a9fef5cba32129  akonadi-mime-23.04.1.tar.xz
"
