# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=filelight
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/filelight"
pkgdesc="An application to visualize the disk usage on your computer"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/filelight-$pkgver.tar.xz
	0001-Fix-musl-build.patch
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b91c833ddd028a59ccc6ca35e76e45dd5db09bba6bb2f749df63264b16bd3c4d1364e1ac0e9573bc4dda85a9a51015eb3c685e692d7c0104bd3a6b633878ff6c  filelight-23.04.1.tar.xz
db0d2fe06a3073c981814989edbaa97cd8b14fa05894c735273b30fc3f95c69207db22bd87db31a703d8f187d1b65827663d60192ba979b977ad51d4cdaa37ad  0001-Fix-musl-build.patch
"
