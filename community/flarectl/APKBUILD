# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=flarectl
pkgver=0.67.0
pkgrel=0
pkgdesc="CLI application for interacting with a Cloudflare account"
url="https://github.com/cloudflare/cloudflare-go/tree/master/cmd/flarectl"
arch="all"
license="BSD-3-Clause"
makedepends="go"
source="https://github.com/cloudflare/cloudflare-go/archive/refs/tags/v$pkgver/flarectl-$pkgver.tar.gz"
builddir="$srcdir/cloudflare-go-$pkgver/cmd/flarectl"
options="!check chmod-clean net" # no testsuite provided

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -buildvcs=false"

build() {
	go build -o flarectl
}

package() {
	install -Dm755 flarectl -t "$pkgdir"/usr/bin
}

sha512sums="
3d8bd7f23ca6279756c8664adb22ee949ac234205c83805c1fd20556a7b3fbde27485a2e8a9a8746a61f9f25cb6dbb609f5bd93180fa70c5c3e342f1f227dbaa  flarectl-0.67.0.tar.gz
"
