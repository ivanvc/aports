# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaccounts-integration
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by signon-ui -> qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	accounts-qml-module
	signon-ui
	"
depends_dev="
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	libaccounts-qt-dev
	qcoro-dev
	qt5-qtbase-dev
	signond-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-integration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2ad4636d6d7c34dabe4c52f6f156525ba715b2665b0c8843a5ed3ba139aad8cc42e1093025d0b5ca3ed7d1c6573c14b3f022cdd7507ec779e6e7c4072d35a3ae  kaccounts-integration-23.04.1.tar.xz
"
