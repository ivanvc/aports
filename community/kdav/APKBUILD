# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdav
pkgver=5.106.0
pkgrel=0
pkgdesc="A DAV protocol implementation with KJobs"
url="https://community.kde.org/Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.0-or-later"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	qt5-qtxmlpatterns-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdav-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2f7f12d1f44d796be98a6cc265d17b0cc87981e1af7f637c84174ecc38656263dcb0f2f1b41f537bfd6db4fe8a89f77ee0b8940e1e4231206dbcb7b9f67a67ec  kdav-5.106.0.tar.xz
"
