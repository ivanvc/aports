# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-xlsxwriter
pkgver=3.1.1
pkgrel=0
pkgdesc="A Python module for creating Excel XLSX files"
url="https://github.com/jmcnamara/XlsxWriter"
arch="noarch"
license="BSD-2-Clause"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jmcnamara/XlsxWriter/archive/RELEASE_$pkgver.tar.gz"
builddir="$srcdir"/XlsxWriter-RELEASE_$pkgver

replaces=py-xlsxwriter # Backwards compatibility
provides=py-xlsxwriter=$pkgver-r$pkgrel # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest -n 2
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
c827bdaefd962864e09673b891ec267da89306f65ea29ccab1807af8ed89f22721d6ca296b7b83d4099bdeadf4508f576e6e618828ffc8abed1d38b3c84c6076  py3-xlsxwriter-3.1.1.tar.gz
"
