# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgoldrunner
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kgoldrunner/"
pkgdesc="A game of action and puzzle solving"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgoldrunner-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f4ae63fb9c9ac621a5ec330d031120f2635852bafc49f7e236c914f3fe9bcecafff934ad82526eb967fd3644f63bcce7ca0d149cb1d18d977be90410bc1006cd  kgoldrunner-23.04.1.tar.xz
"
