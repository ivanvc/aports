# Contributor: Dmitry Zakharchenko <dmitz@disroot.org>
# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=pipe-viewer
pkgver=0.4.6
pkgrel=0
pkgdesc="Lightweight YouTube client that does not require an YouTube API key"
url="https://github.com/trizen/pipe-viewer"
arch="noarch"
license="Artistic-2.0"
makedepends="perl-module-build"
depends="
	perl-data-dump
	perl-json
	perl-libwww
	perl-lwp-protocol-https
	perl-term-readline-gnu
	perl-unicode-linebreak
	"
checkdepends="perl-test-pod"
subpackages="$pkgname-doc $pkgname-gtk"
source="$pkgname-$pkgver.tar.gz::https://github.com/trizen/pipe-viewer/archive/$pkgver.tar.gz"

build() {
	perl Build.PL --gtk3
}

check() {
	./Build test
}

package() {
	./Build install --destdir "$pkgdir" --installdirs vendor
}

gtk() {
	depends="$pkgname perl-gtk3 perl-file-sharedir"
	pkgdesc="$pkgdesc (GTK interface)"

	amove usr/bin/gtk-pipe-viewer
	amove "usr/share/perl5/vendor_perl/auto/share/dist/WWW-PipeViewer/gtk-*"
	amove usr/share/perl5/vendor_perl/auto/share/dist/WWW-PipeViewer/icons
}

sha512sums="
a561ec2973ec527dfe0335def804b107fb13b8f3f21e63a94e566bb9a1f55c31d6bf05d83b632ab4b52ef9e38994be69fa95347aa8c58313b1a078dd8728947b  pipe-viewer-0.4.6.tar.gz
"
