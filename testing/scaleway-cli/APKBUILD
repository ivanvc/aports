# Contributor: Thomas Kienlen <kommander@laposte.net>
# Maintainer: Thomas Kienlen <kommander@laposte.net>
pkgname=scaleway-cli
pkgver=2.14.0
pkgrel=1
pkgdesc="Command-line client for Scaleway Cloud"
url="https://www.scaleway.com/en/cli"
arch="all !x86 !armv7 !armhf" # tests are failing for x86, armv7, armhf
license="Apache-2.0"
makedepends="go"
source="scaleway-cli-$pkgver.tar.gz::https://github.com/scaleway/scaleway-cli/archive/refs/tags/v$pkgver.tar.gz"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="chmod-clean"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o scw ./cmd/scw/main.go
	PATH=. scw autocomplete script shell=bash > bashcomp
	PATH=. scw autocomplete script shell=fish > fishcomp
	PATH=. scw autocomplete script shell=zsh > zshcomp
}

check() {
	go test -v ./...
}

package() {
	install -Dm755 scw "$pkgdir"/usr/bin/scw

	install -Dm644 bashcomp "$pkgdir"/usr/share/bash-completion/completions/scw
	install -Dm644 fishcomp "$pkgdir"/usr/share/fish/completions/scw.fish
	install -Dm644 zshcomp "$pkgdir"/usr/share/zsh/site-functions/_scw

}

sha512sums="
d9fc412bce60f76f198acd3cdb881fe91e056f33a3155c030147fcf0d439ce12f1bb2c1ddc73494551b652eebb294fb40a573e082ba4befdcf1c6c7b11d0f871  scaleway-cli-2.14.0.tar.gz
"
