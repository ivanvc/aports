# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=ormolu
pkgver=0.6.0.1
pkgrel=0
pkgdesc="Formatter for Haskell source code"
url="https://github.com/tweag/ormolu"
arch="x86_64 aarch64" # limited by ghc
license="BSD-3-Clause"
makedepends="ghc cabal"
options="net"
source="https://hackage.haskell.org/package/ormolu-$pkgver/ormolu-$pkgver.tar.gz
	cabal.project.freeze"

# Directory were cabal files are stored.
export CABAL_DIR="$srcdir/dist"

# Needed to fix build on aarch64.
export PATH="$PATH:/usr/lib/llvm14/bin"

cabal_update() {
	cd $builddir
	cabal v2-update
	(
		cd "$builddir"
		cabal v2-freeze --shadow-installed-packages
		mv cabal.project.freeze "$startdir/"
	)
}

prepare() {
	default_prepare
	ln -sf "$srcdir/cabal.project.freeze" \
		"$builddir/cabal.project.freeze"
}

build() {
	cabal v2-update
	cabal v2-build ormolu:exes \
		--jobs=${JOBS:-1} \
		--prefix=/usr \
		--docdir=/usr/share/doc/$pkgname \
		--sysconfdir=/etc
}

package() {
	# See https://github.com/haskell/cabal/issues/6919#issuecomment-761563498
	cabal list-bin ormolu:exes | xargs install -Dm755 -t "$pkgdir"/usr/bin
}

sha512sums="
32577cf3813833009ad7ed42c630b29f626aa2b1621e0fed4a33b1fe29671893b7576b5dde5e585acea0a3846048bdb5e8d5661a08a7a0cb435b9506cc360c9f  ormolu-0.6.0.1.tar.gz
954f447b700e6fd11b1a51e7238552f792ce291d3a081782c406b5d6a688107c25a42d119054b4c45978307c53675820140bcdfc13a231d9edbfb41c1b97f062  cabal.project.freeze
"
