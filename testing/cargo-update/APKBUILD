# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=cargo-update
pkgver=13.0.4
pkgrel=0
pkgdesc="cargo subcommand for checking and applying updates to installed executables"
url="https://github.com/nabijaczleweli/cargo-update"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable ronn curl-dev libgit2-dev libssh-dev"
subpackages="$pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/nabijaczleweli/cargo-update/archive/refs/tags/v$pkgver.tar.gz
	"

[ "$CARCH" = "riscv64" ] && options="$options textrels"

export LIBSSH2_SYS_USE_PKG_CONFIG=1

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET"
}

build() {
	cargo auditable build --frozen --release
	ronn --roff --organization="CARGO-UPDATE $pkgver" man/*.md
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin/ \
		target/release/cargo-install-update \
		target/release/cargo-install-update-config

	install -Dm644 man/cargo-install-update*.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
182d4a84915ac030eea8493b643b87968731c313d8f392ca29b4ffa2fb209126caa76458fb5e042c925363abb47757bc0ab2df3f8fc7854b0a377777915a6012  cargo-update-13.0.4.tar.gz
"
