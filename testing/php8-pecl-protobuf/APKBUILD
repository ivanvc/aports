# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php8-pecl-protobuf
_extname=protobuf
pkgver=3.23.1
pkgrel=0
pkgdesc="PHP 8.0 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data - PECL"
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
_phpv=8
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Test suite is not a part of pecl release.
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
60d27e0cfe03a550395c20c525167c58a39fe245f5d7947a10cd2a92d0e0c2cd6226c9fa43a79191816e2bc41dc05d95dcab5969f294b4d8ca74c4deeed2f77b  php-pecl-protobuf-3.23.1.tgz
"
