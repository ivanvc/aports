# Maintainer: psykose <alice@ayaya.dev>
pkgname=libclc
# follow llvm versioning
pkgver=16.0.4
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="Open source implementation of the library requirements of the OpenCL C programming language"
url="https://libclc.llvm.org/"
arch="all"
license="Apache-2.0 WITH LLVM-exception"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	clang-dev
	cmake
	llvm$_llvmver-dev
	llvm$_llvmver-static
	samurai
	spirv-llvm-translator-dev
	"
checkdepends="llvm-test-utils"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/libclc-$pkgver.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/cmake-$pkgver.src.tar.xz
	"
builddir="$srcdir/libclc-$pkgver.src"
options="!check" # todo: they fail for some reason

build() {
	CC=clang \
	CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLLVM_SPIRV=/usr/bin/llvm-spirv \
		-DCMAKE_MODULE_PATH="$srcdir/cmake-$pkgver.src/Modules"
	cmake --build build
}

check() {
	cmake --build build --target test
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a3ddce2d880bfdc09ff1e518e9ed093653f227901641221d3fc069d39a4a9a671806c395e71667b22c6cfc8faba6772d6436e5f65a40fbb575cb46bacf442879  libclc-16.0.4.src.tar.xz
942f10a5d1e3e48768d62a2595f8670872069ab2065871c786a435ae23108fb263e8c3db906dca0e68aeb8aad00f62f7604cd2f41da9e00f574b6021f846bb9d  cmake-16.0.4.src.tar.xz
"
